package com.field.order.manager.domain.assignment;

import com.field.order.manager.domain.order.OrderRef;
import com.field.order.manager.domain.provider.ServiceProviderRef;
import lombok.Data;

import java.util.Date;

/**
 * Created by sanemdeepak on 11/30/18.
 */
@Data
public class OrderAssignmentInfo {
    private OrderRef orderRef;
    private ServiceProviderRef serviceProviderRef;
    private Date assignedOn;
    private Date lastUpdatedOn;
    private Date closedOn;
}
