package com.field.order.manager.domain.provider;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Data
public class ServiceProvider {
    private UUID id;

    @JsonIgnore
    private Boolean active;

    @JsonIgnore
    public void setId(UUID id) {
        this.id = id;
    }

    @JsonProperty
    public UUID getId() {
        return this.id;
    }



    private ServiceProviderInfo info;
}
