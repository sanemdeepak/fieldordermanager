package com.field.order.manager.domain;

/**
 * Created by sanemdeepak on 11/15/18.
 */
public enum  OrderStatus {
    RECEIVED,
    ACKNOWLEDGED,
    ASSIGNED,
    PROCESSING,
    ASSIGNMENT_REJECTED,
    PENDING_ASSIGNMENT,
    PENDING_INVENTORY,
    PENDING_ASSISTANCE,
    CLOSED_COMPLETE,
    CLOSED_CANCELD,
    CLOSED_INCOMPLETE,
    READY_FOR_REVIEW,
    REJECTED_INTERNAL,
    REJECTED_CLIENT,
}
