package com.field.order.manager.domain.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/15/18.
 */

@Data
public class Order {
    private UUID id;

    @JsonIgnore
    public void setId(UUID id) {
        this.id = id;
    }

    @JsonProperty
    public UUID getId() {
        return this.id;
    }

    @Valid
    private OrderInfo info;
}
