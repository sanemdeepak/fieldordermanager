package com.field.order.manager.domain.address;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Created by sanemdeepak on 11/27/18.
 */
@Data
public class Address {

    @NotBlank
    @Size(max = 25)
    private String lineOne;
    
    @Size(max = 25)
    private String lineTwo;

    @NotBlank
    @Size(max = 25)
    private String city;

    @NotBlank
    @Size(min = 6, max = 6)
    private String pin;

    private State state;
}
