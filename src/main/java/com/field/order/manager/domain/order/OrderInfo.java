package com.field.order.manager.domain.order;

import com.field.order.manager.domain.address.Address;
import com.field.order.manager.domain.customer.CustomerRef;
import com.field.order.manager.domain.provider.ServiceType;
import lombok.Data;

/**
 * Created by sanemdeepak on 11/16/18.
 */
@Data
public class OrderInfo {
    private Long number;
    private CustomerRef forCustomer;
    private ServiceType serviceType;
    private Address addressForOrder;
    private String serviceDesc;
    private String additionalContactName;
    private String additionalContactNumber;
}
