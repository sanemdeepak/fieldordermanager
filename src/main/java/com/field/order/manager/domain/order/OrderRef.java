package com.field.order.manager.domain.order;

import lombok.Data;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/15/18.
 */
@Data
public class OrderRef {
    private UUID orderId;
}
