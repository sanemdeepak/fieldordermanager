package com.field.order.manager.domain.provider;

import com.field.order.manager.domain.address.Address;
import lombok.Data;

import java.util.List;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Data
public class ServiceProviderInfo {
    private Long number;
    private String name;
    private Address address;
    private List<ServiceType> serviceType;
    private String phoneNumber;
    private String contactPerson;
}
