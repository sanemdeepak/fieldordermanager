package com.field.order.manager.domain.order;

/**
 * Created by sanemdeepak on 11/27/18.
 */
public enum OrderType {
    AIRCONDITIONING,
    ELECTRICAL,
    PLUMBING,
    HOUSEKEEPING
}
