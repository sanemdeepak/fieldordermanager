package com.field.order.manager.domain.customer;

import com.field.order.manager.domain.address.Address;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Data
public class CustomerInfo {
    private Long number;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @Valid
    private Address address;
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String email;
}
