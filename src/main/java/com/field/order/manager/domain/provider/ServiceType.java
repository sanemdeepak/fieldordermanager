package com.field.order.manager.domain.provider;

/**
 * Created by sanemdeepak on 11/28/18.
 */
public enum ServiceType {
    ELECTRICAL,
    PLUMBING,
    HOUSEKEEPING,
    EVENTORGANIZING
}
