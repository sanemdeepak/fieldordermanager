package com.field.order.manager.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * Created by sanemdeepak on 11/15/18.
 */
@Data
@AllArgsConstructor
public class OrderStatusEntry {
    private OrderStatus orderStatus;
    private Date createdOn;
}
