package com.field.order.manager.domain.provider;

import lombok.Data;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Data
public class ServiceProviderRef {
    private UUID id;
}
