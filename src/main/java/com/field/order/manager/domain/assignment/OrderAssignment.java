package com.field.order.manager.domain.assignment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/15/18.
 */
@Data
public class OrderAssignment {
    private UUID id;

    @JsonIgnore
    public void setId(UUID id) {
        this.id = id;
    }

    @JsonProperty
    public UUID getId() {
        return this.id;
    }

    private OrderAssignmentInfo info;
}
