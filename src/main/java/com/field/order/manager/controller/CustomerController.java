package com.field.order.manager.controller;

import com.field.order.manager.domain.customer.Customer;
import com.field.order.manager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
@Slf4j
public class CustomerController {

    private final CustomerService customerService;


    @GetMapping("/{id}")
    public Customer getById(@PathVariable("id") UUID id) {
        return customerService.getById(id);
    }

    @GetMapping("/number/{number}")
    public Customer getByNumber(@PathVariable("number") Long number) {
        return customerService.getByNumber(number);
    }

    @PostMapping
    public Customer create(@RequestBody @Valid Customer customer) {
        return customerService.create(customer);
    }
}
