package com.field.order.manager.controller;

import com.field.order.manager.domain.provider.ServiceProvider;
import com.field.order.manager.domain.provider.ServiceType;
import com.field.order.manager.service.ServiceProviderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@RestController
@RequestMapping("/provider")
@RequiredArgsConstructor
@Slf4j
public class ServiceProviderController {

    private final ServiceProviderService serviceProviderService;

    @GetMapping("/{id}")
    public ServiceProvider getById(@PathVariable("id") UUID id) {
        return serviceProviderService.getById(id);
    }

    @GetMapping("/number/{number}")
    public ServiceProvider getByNumber(@PathVariable("number") Long number) {
        return serviceProviderService.getByNumber(number);
    }

    @PostMapping
    public ServiceProvider create(@RequestBody @Valid ServiceProvider serviceProvider) {
        return serviceProviderService.create(serviceProvider);
    }

    @GetMapping("/pin/{pin}/type/{type}")
    public List<ServiceProvider> searchByPinAndType(@PathVariable("pin") String pin, @PathVariable("type") ServiceType serviceType){
        return serviceProviderService.getAllByPinAndType(pin, serviceType);
    }
}
