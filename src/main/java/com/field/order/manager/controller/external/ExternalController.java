package com.field.order.manager.controller.external;

import com.field.order.manager.executor.ServiceExecutor;
import com.field.order.manager.executor.dto.ExecutorDomain;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by sanemdeepak on 11/30/18.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/external")
public class ExternalController {
    private final ServiceExecutor serviceExecutor;

    @PostMapping("/order")
    public Long makeOrder(@RequestBody @Valid ExecutorDomain executorDomain) {
        return serviceExecutor.makeOrder(executorDomain.getCustomer(), executorDomain.getOrder());
    }

}
