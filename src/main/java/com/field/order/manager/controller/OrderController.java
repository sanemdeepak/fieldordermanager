package com.field.order.manager.controller;

import com.field.order.manager.domain.order.Order;
import com.field.order.manager.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/30/18.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderController {
    private final OrderService orderService;


    @GetMapping("/{id}")
    public Order getById(@PathVariable("id") UUID id) {
        return orderService.getById(id);
    }

    @GetMapping("/number/{number}")
    public Order getByNumber(@PathVariable("number") Long number) {
        return orderService.getByNumber(number);
    }


    @PostMapping
    public Order create(@RequestBody @Valid Order order) {
        return orderService.create(order);
    }
}
