package com.field.order.manager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by sanemdeepak on 11/16/18.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ParameterRequiredException extends RuntimeException {

    public ParameterRequiredException() {
        super();
    }

    public ParameterRequiredException(String message) {
        super(message);
    }
}
