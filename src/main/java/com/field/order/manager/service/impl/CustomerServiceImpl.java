package com.field.order.manager.service.impl;

import com.field.order.manager.domain.customer.Customer;
import com.field.order.manager.exception.ParameterRequiredException;
import com.field.order.manager.exception.ResourceNotFoundException;
import com.field.order.manager.repo.CustomerRepo;
import com.field.order.manager.service.CustomerService;
import com.field.order.manager.service.PreProcessor;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepo customerRepo;
    private final PreProcessor preProcessor;


    @Override
    public Customer create(Customer customer) {
        if (customer == null || customer.getInfo() == null) {
            throw new ParameterRequiredException();
        }
        customer = preProcessor.process(customer);
        customerRepo.create(customer);
        return getByNumber(customer.getInfo().getNumber());
    }

    @Override
    public Customer getById(UUID id) {
        return customerRepo.getById(id);
    }

    @Override
    public Customer getByNumber(Long number) {
        return customerRepo.getByNumber(number);
    }

    @Override
    public Optional<Customer> getByPhoneNumber(String phoneNumber) {
        try {
            return Optional.of(customerRepo.getByPhoneNumber(phoneNumber));
        } catch (DataAccessException exp) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Customer> getByEmail(String email) {
        try {
            return Optional.of(customerRepo.getByEmail(email));
        } catch (DataAccessException exp) {
            return Optional.empty();
        }
    }

    @Override
    public Customer getByPhoneNumberOrEmail(String phoneNumber, String email) {
        return getByPhoneNumber(phoneNumber)
                .orElse(getByEmail(email)
                        .orElseThrow(ResourceNotFoundException::new));

    }
}
