package com.field.order.manager.service;

import com.field.order.manager.domain.customer.Customer;
import com.field.order.manager.domain.order.Order;
import com.field.order.manager.domain.provider.ServiceProvider;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

/**
 * Created by sanemdeepak on 11/16/18.
 */
@Component
public class PreProcessor {

    private final SecureRandom secureRandom;

    public PreProcessor() {
        try {
            this.secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (Exception exp) {
            throw new RuntimeException(exp);
        }
    }

    private Long getRandomLong() {
        return (long) (10000 + secureRandom.nextInt(90000));
    }


    public ServiceProvider process(ServiceProvider serviceProvider) {
        serviceProvider.getInfo().setNumber(getRandomLong());
        return serviceProvider;
    }

    public Customer process(Customer customer) {
        customer.getInfo().setNumber(getRandomLong());
        return customer;
    }

    public Order process(Order order) {
        order.getInfo().setNumber(getRandomLong());
        return order;
    }
}
