package com.field.order.manager.service;

import com.field.order.manager.domain.customer.Customer;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
public interface CustomerService {

    Customer create(Customer customer);

    Customer getById(UUID id);

    Customer getByNumber(Long number);

    Optional<Customer> getByPhoneNumber(String phoneNumber);

    Optional<Customer> getByEmail(String email);

    Customer getByPhoneNumberOrEmail(String phoneNumber, String email);
}
