package com.field.order.manager.service.impl;

import com.field.order.manager.domain.assignment.OrderAssignment;
import com.field.order.manager.domain.assignment.OrderAssignmentInfo;
import com.field.order.manager.domain.order.Order;
import com.field.order.manager.domain.order.OrderInfo;
import com.field.order.manager.domain.order.OrderRef;
import com.field.order.manager.domain.provider.ServiceProvider;
import com.field.order.manager.domain.provider.ServiceProviderRef;
import com.field.order.manager.domain.provider.ServiceType;
import com.field.order.manager.exception.ParameterRequiredException;
import com.field.order.manager.repo.OrderAssignmentRepo;
import com.field.order.manager.service.OrderAssignmentService;
import com.field.order.manager.service.ServiceProviderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/30/18.
 */
@Service
@RequiredArgsConstructor
public class OrderAssignmentServiceImpl implements OrderAssignmentService {

    private final OrderAssignmentRepo orderAssignmentRepo;
    private final ServiceProviderService serviceProviderService;

    @Override
    public OrderAssignment create(Order order) {
        if (order == null || order.getId() == null || order.getInfo() == null) {
            throw new ParameterRequiredException();
        }
        OrderRef orderRef = new OrderRef();
        orderRef.setOrderId(order.getId());

        OrderInfo orderInfo = order.getInfo();
        ServiceType serviceType = orderInfo.getServiceType();
        String pin = orderInfo.getAddressForOrder().getPin();

        List<ServiceProvider> serviceProviders = serviceProviderService.getAllByPinAndType(pin, serviceType);

        if (serviceProviders.isEmpty()) {
            throw new RuntimeException("Service Not available in your area");
        }

        int randomIdx = randomNumber(serviceProviders.size()) - 1;

        if (randomIdx < 0) {
            randomIdx = randomIdx + 1;
        }

        ServiceProviderRef serviceProviderRef = new ServiceProviderRef();
        serviceProviderRef.setId(serviceProviders.get(randomIdx).getId());

        OrderAssignment assignment = new OrderAssignment();
        UUID assignmentId = UUID.randomUUID();
        assignment.setId(assignmentId);

        OrderAssignmentInfo orderAssignmentInfo = new OrderAssignmentInfo();
        orderAssignmentInfo.setOrderRef(orderRef);
        orderAssignmentInfo.setServiceProviderRef(serviceProviderRef);
        orderAssignmentInfo.setAssignedOn(Date.from(Instant.now()));
        orderAssignmentInfo.setLastUpdatedOn(Date.from(Instant.now()));

        assignment.setInfo(orderAssignmentInfo);

        orderAssignmentRepo.create(assignment);

        return getById(assignmentId);
    }

    @Override
    public OrderAssignment getById(UUID id) {
        return null;
    }


    private int randomNumber(int max) {
        return new SecureRandom().nextInt(max);
    }
}
