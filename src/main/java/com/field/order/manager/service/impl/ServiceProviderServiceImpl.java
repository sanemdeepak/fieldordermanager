package com.field.order.manager.service.impl;

import com.field.order.manager.domain.provider.ServiceProvider;
import com.field.order.manager.domain.provider.ServiceType;
import com.field.order.manager.exception.ParameterRequiredException;
import com.field.order.manager.repo.ServiceProviderRepo;
import com.field.order.manager.service.PreProcessor;
import com.field.order.manager.service.ServiceProviderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ServiceProviderServiceImpl implements ServiceProviderService {
    private final ServiceProviderRepo serviceProviderRepo;
    private final PreProcessor preProcessor;

    @Override
    public ServiceProvider create(ServiceProvider serviceProvider) {
        if (serviceProvider == null || serviceProvider.getInfo() == null) {
            throw new ParameterRequiredException();
        }
        serviceProvider = preProcessor.process(serviceProvider);
        serviceProviderRepo.create(serviceProvider);
        return getByNumber(serviceProvider.getInfo().getNumber());
    }

    @Override
    public ServiceProvider getById(UUID id) {
        return serviceProviderRepo.getById(id);
    }

    @Override
    public ServiceProvider getByNumber(Long number) {
        return serviceProviderRepo.getByNumber(number);
    }

    @Override
    public List<ServiceProvider> getAllByPinAndType(String pin, ServiceType serviceType) {
        if (StringUtils.isBlank(pin) || serviceType == null) {
            return Collections.emptyList();
        }
        return serviceProviderRepo.getAllBy(pin, serviceType);
    }
}
