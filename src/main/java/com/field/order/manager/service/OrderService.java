package com.field.order.manager.service;

import com.field.order.manager.domain.order.Order;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/29/18.
 */
public interface OrderService {
    Order create(Order order);

    Order getById(UUID id);

    Order getByNumber(Long number);
}
