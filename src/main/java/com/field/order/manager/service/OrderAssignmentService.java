package com.field.order.manager.service;

import com.field.order.manager.domain.assignment.OrderAssignment;
import com.field.order.manager.domain.order.Order;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/30/18.
 */
public interface OrderAssignmentService {
    OrderAssignment create(Order order);

    OrderAssignment getById(UUID id);
}
