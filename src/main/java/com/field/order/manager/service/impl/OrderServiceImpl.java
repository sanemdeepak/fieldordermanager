package com.field.order.manager.service.impl;

import com.field.order.manager.domain.order.Order;
import com.field.order.manager.exception.ParameterRequiredException;
import com.field.order.manager.repo.OrderRepo;
import com.field.order.manager.service.OrderAssignmentService;
import com.field.order.manager.service.OrderService;
import com.field.order.manager.service.PreProcessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/29/18.
 */
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepo orderRepo;
    private final PreProcessor preProcessor;

    private final OrderAssignmentService orderAssignmentService;


    @Override
    public Order create(Order order) {
        if (order == null || order.getInfo() == null) {
            throw new ParameterRequiredException();
        }
        order = preProcessor.process(order);
        orderRepo.create(order);

        Order newOrder = getByNumber(order.getInfo().getNumber());
        orderAssignmentService.create(newOrder);
        return newOrder;
    }

    @Override
    public Order getById(UUID id) {
        return orderRepo.getById(id);
    }

    @Override
    public Order getByNumber(Long number) {
        return orderRepo.getByNumber(number);
    }
}
