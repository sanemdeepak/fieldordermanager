package com.field.order.manager.executor;

import com.field.order.manager.domain.customer.Customer;
import com.field.order.manager.domain.customer.CustomerRef;
import com.field.order.manager.domain.order.Order;
import com.field.order.manager.service.CustomerService;
import com.field.order.manager.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

/**
 * Created by sanemdeepak on 11/30/18.
 */
@Service
@RequiredArgsConstructor
public class ServiceExecutor {
    private final CustomerService customerService;
    private final OrderService orderService;


    public Long makeOrder(Customer customer, Order order) {

        Customer processedCustomer = createIfNotExist(customer);

        CustomerRef customerRef = new CustomerRef();
        customerRef.setId(processedCustomer.getId());

        order.getInfo().setForCustomer(customerRef);
        Order processedOrder = orderService.create(order);

        return processedOrder.getInfo().getNumber();
    }

    private Customer createIfNotExist(Customer customer) {
        try {
            return customerService.create(customer);
        } catch (DuplicateKeyException exp) {
            return customerService.getByPhoneNumberOrEmail(customer.getInfo().getPhoneNumber(), customer.getInfo().getEmail());
        }
    }
}
