package com.field.order.manager.executor.dto;

import com.field.order.manager.domain.customer.Customer;
import com.field.order.manager.domain.order.Order;
import lombok.Data;

import javax.validation.Valid;

/**
 * Created by sanemdeepak on 11/30/18.
 */
@Data
public class ExecutorDomain {
    @Valid
    private Customer customer;
    @Valid
    private Order order;
}
