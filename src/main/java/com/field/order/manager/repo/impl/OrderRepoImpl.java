package com.field.order.manager.repo.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.field.order.manager.common.Util;
import com.field.order.manager.domain.order.Order;
import com.field.order.manager.domain.order.OrderInfo;
import com.field.order.manager.repo.OrderRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/22/18.
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class OrderRepoImpl implements OrderRepo {
    private final JdbcTemplate database;
    private final ObjectMapper mapper;
    private final Util util;

    private final String TABLE_NAME = "order";

    @Override
    public Order create(Order order) {
        String command = String.format("INSERT INTO \"%s\"(info) VALUES(?::JSON)", TABLE_NAME);
        String json = util.getObjectAsString(order.getInfo());
        database.update(command, json);
        return order;
    }

    @Override
    public Order getById(UUID id) {
        String query = String.format("SELECT * FROM \"%s\" WHERE id = ?", TABLE_NAME);
        return database.queryForObject(query, new Object[]{id}, this::convertToOrder);
    }

    @Override
    public Order getByNumber(Long number) {
        String query = String.format("SELECT * FROM \"%s\" WHERE info ->>'number' = ?", TABLE_NAME);
        return database.queryForObject(query, new Object[]{number.toString()}, this::convertToOrder);
    }

    private Order convertToOrder(ResultSet resultSet, int i) {
        try {
            String info = resultSet.getString("info");
            UUID id = UUID.fromString(resultSet.getString("id"));
            OrderInfo orderInfo = mapper.readValue(info, OrderInfo.class);

            Order order = new Order();
            order.setId(id);
            order.setInfo(orderInfo);

            return order;
        } catch (Exception exp) {
            throw new RuntimeException(exp);
        }
    }
}
