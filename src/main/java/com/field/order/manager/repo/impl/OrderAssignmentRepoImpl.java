package com.field.order.manager.repo.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.field.order.manager.common.Util;
import com.field.order.manager.domain.assignment.OrderAssignment;
import com.field.order.manager.domain.assignment.OrderAssignmentInfo;
import com.field.order.manager.repo.OrderAssignmentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/30/18.
 */
@Repository
@RequiredArgsConstructor
public class OrderAssignmentRepoImpl implements OrderAssignmentRepo {

    private final JdbcTemplate database;
    private final ObjectMapper mapper;
    private final Util util;


    private String TABLE_NAME = "order_assignment";


    @Override
    public OrderAssignment create(OrderAssignment orderAssignment) {
        String command = String.format("INSERT INTO %s(id,info) VALUES(?,?::JSON)", TABLE_NAME);
        String info = util.getObjectAsString(orderAssignment.getInfo());
        UUID id = UUID.randomUUID();
        database.update(command, id, info);
        return getById(id);
    }

    @Override
    public OrderAssignment update(UUID id, OrderAssignment assignment) {
        return null;
    }

    @Override
    public OrderAssignment getById(UUID id) {
        String query = String.format("SELECT * FROM %s WHERE id = ?", TABLE_NAME);
        return database.queryForObject(query, this::toOrderAssignment, id);
    }


    private OrderAssignment toOrderAssignment(ResultSet rs, int idx) {
        try {
            UUID id = UUID.fromString(rs.getString("id"));
            String infoAsStr = rs.getString("info");
            OrderAssignmentInfo info = mapper.readValue(infoAsStr, OrderAssignmentInfo.class);

            OrderAssignment assignment = new OrderAssignment();
            assignment.setId(id);
            assignment.setInfo(info);
            return assignment;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
