package com.field.order.manager.repo;

import com.field.order.manager.domain.customer.Customer;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
public interface CustomerRepo {

    Customer create(Customer customer);

    Customer getById(UUID id);

    Customer getByNumber(Long number);

    Customer getByPhoneNumber(String phoneNumber);

    Customer getByEmail(String email);
}
