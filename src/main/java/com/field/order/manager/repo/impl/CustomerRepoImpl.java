package com.field.order.manager.repo.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.field.order.manager.common.Util;
import com.field.order.manager.domain.customer.Customer;
import com.field.order.manager.domain.customer.CustomerInfo;
import com.field.order.manager.exception.ResourceNotFoundException;
import com.field.order.manager.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Repository
@RequiredArgsConstructor
public class CustomerRepoImpl implements CustomerRepo {
    private final JdbcTemplate database;
    private final ObjectMapper mapper;
    private final Util util;

    private final String TABLE_NAME = "customer";

    @Override
    public Customer create(Customer customer) {
        String command = String.format("INSERT INTO %s(info) VALUES(?::JSON)", TABLE_NAME);
        String json = util.getObjectAsString(customer.getInfo());
        database.update(command, json);
        return customer;
    }

    @Override
    public Customer getById(UUID id) {
        String query = String.format("SELECT * FROM %s WHERE id = ?", TABLE_NAME);
        return getCustomer(query, id);
    }

    @Override
    public Customer getByNumber(Long number) {
        String query = String.format("SELECT * FROM %s WHERE info ->> 'number' = ?", TABLE_NAME);
        return getCustomer(query, number.toString());
    }

    @Override
    public Customer getByPhoneNumber(String phoneNumber) {
        String query = String.format("SELECT * FROM %s WHERE info ->> 'phoneNumber' = ?", TABLE_NAME);
        return getCustomer(query, phoneNumber);
    }

    @Override
    public Customer getByEmail(String email) {
        String query = String.format("SELECT * FROM %s WHERE info ->> 'email' = ?", TABLE_NAME);
        return getCustomer(query, email);
    }

    private Customer convertResultSetToCustomer(ResultSet rs, int index) {
        try {
            String info = rs.getString("info");
            UUID id = UUID.fromString(rs.getString("id"));
            CustomerInfo customerInfo = mapper.readValue(info, CustomerInfo.class);

            Customer customer = new Customer();
            customer.setId(id);
            customer.setInfo(customerInfo);
            return customer;
        } catch (Exception exp) {
            throw new RuntimeException(exp);
        }
    }


    private Customer getCustomer(String query, Object... args) {
        try {
            return database.queryForObject(query, this::convertResultSetToCustomer, args);
        } catch (DataAccessException exp) {
            if (exp instanceof IncorrectResultSizeDataAccessException) {
                throw new ResourceNotFoundException();
            }
            throw exp;
        }
    }
}
