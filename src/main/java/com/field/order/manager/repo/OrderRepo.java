package com.field.order.manager.repo;

import com.field.order.manager.domain.order.Order;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/22/18.
 */
public interface OrderRepo {
    Order create(Order order);

    Order getById(UUID id);

    Order getByNumber(Long number);

}
