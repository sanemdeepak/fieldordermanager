package com.field.order.manager.repo.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.field.order.manager.common.Util;
import com.field.order.manager.domain.provider.ServiceProvider;
import com.field.order.manager.domain.provider.ServiceProviderInfo;
import com.field.order.manager.domain.provider.ServiceType;
import com.field.order.manager.repo.ServiceProviderRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Repository
@RequiredArgsConstructor
public class ServiceProviderRepoImpl implements ServiceProviderRepo {

    private final JdbcTemplate database;
    private final Util util;
    private final ObjectMapper mapper;

    private final String TABLE_NAME = "service_provider";

    @Override
    public ServiceProvider create(ServiceProvider serviceProvider) {
        String command = String.format("INSERT INTO %s(active, info) VALUES(?,?::JSON)", TABLE_NAME);
        String json = util.getObjectAsString(serviceProvider.getInfo());
        database.update(command, Boolean.TRUE, json);
        return serviceProvider;
    }

    @Override
    public ServiceProvider getById(UUID id) {
        String query = String.format("SELECT * FROM %s WHERE id = ?", TABLE_NAME);
        return database.queryForObject(query, new Object[]{id}, this::convertResultSetToServiceProvider);
    }

    @Override
    public ServiceProvider getByNumber(Long number) {
        String query = String.format("SELECT * FROM %s WHERE info ->> 'number' = ?", TABLE_NAME);
        return database.queryForObject(query, new Object[]{number.toString()}, this::convertResultSetToServiceProvider);
    }

    @Override
    public List<ServiceProvider> getAllBy(String pin, ServiceType serviceType) {
        String query = String.format("SELECT * FROM %s WHERE info->'address'->>'pin' = '%s' AND (info->'serviceType')::jsonb ? '%s'", TABLE_NAME, pin, serviceType.toString());
        return database.query(query, this::convertResultSetToServiceProvider);
    }


    private ServiceProvider convertResultSetToServiceProvider(ResultSet rs, int index) {
        try {
            String info = rs.getString("info");
            UUID id = UUID.fromString(rs.getString("id"));
            Boolean active = rs.getBoolean("active");

            ServiceProviderInfo providerInfo = mapper.readValue(info, ServiceProviderInfo.class);

            ServiceProvider provider = new ServiceProvider();
            provider.setId(id);
            provider.setActive(active);
            provider.setInfo(providerInfo);
            return provider;
        } catch (Exception exp) {
            throw new RuntimeException(exp);
        }
    }
}
