package com.field.order.manager.repo;

import com.field.order.manager.domain.provider.ServiceProvider;
import com.field.order.manager.domain.provider.ServiceType;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
public interface ServiceProviderRepo {

    ServiceProvider create(ServiceProvider serviceProvider);

    ServiceProvider getById(UUID id);

    ServiceProvider getByNumber(Long number);

    List<ServiceProvider> getAllBy(String pin, ServiceType serviceType);
}
