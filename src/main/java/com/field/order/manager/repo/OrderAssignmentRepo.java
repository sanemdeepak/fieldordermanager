package com.field.order.manager.repo;

import com.field.order.manager.domain.assignment.OrderAssignment;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/30/18.
 */
public interface OrderAssignmentRepo {
    OrderAssignment create(OrderAssignment orderAssignment);

    OrderAssignment update(UUID id, OrderAssignment assignment);

    OrderAssignment getById(UUID id);
}
