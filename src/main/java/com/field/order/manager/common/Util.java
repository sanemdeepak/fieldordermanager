package com.field.order.manager.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

/**
 * Created by sanemdeepak on 11/16/18.
 */
@Component
public class Util {

    private final ObjectMapper mapper;

    public Util(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public String getObjectAsString(Object obj) {
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
