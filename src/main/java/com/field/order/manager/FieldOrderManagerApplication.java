package com.field.order.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by sanemdeepak on 11/15/18.
 */
@SpringBootApplication
public class FieldOrderManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FieldOrderManagerApplication.class, args);
    }
}
